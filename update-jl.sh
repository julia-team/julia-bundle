#!/bin/sh
set -e

info () {
	echo "[32;1m" $@ "[0;m"
}

create_dch_stub () {
	name=$1
	version=$2
	dest=$3
	cat > $3 <<EOF
$name ($version) UNRELEASED; urgency=medium

  * Initial release. (Closes: #XXXXXX)

 -- Stub <user@example.org>  Fri, 26 Jul 2019 00:18:39 +0000
EOF
}

create_uscan () {
	name=$1
	url=$2
	dest=$3
	cat > $3 <<EOF
version=4
opts="filenamemangle=s%(?:.*?)?v?(\\d[\\d.]*)\\.tar\\.gz%${name}-\$1.tar.gz%" \
${url} \
(?:.*?/)?v?(\\d[\\d.]*)\\.tar\\.gz debian uupdate
EOF
}

mkdir -p pkgs
mkdir -p bundle

for P in $(ls -1 meta); do
	PN=$(echo ${P} | sed -e 's/\.jl//')
	PV="0~"
	if test -r meta/${P}/version; then
		PV=$(cat meta/${P}/version)
	fi
	URL=$(cat meta/${P}/url)

	info "Scanning for ${P} (>> ${PV}) ..."

	mkdir -p pkgs/${P}/debian
	create_uscan ${P} ${URL} pkgs/${P}/debian/watch
	create_dch_stub ${P} ${PV} pkgs/${P}/debian/changelog
	uscan pkgs/${P} || true

	if ! test -r pkgs/${P}-*.tar.gz; then
		continue
	fi

	ln -sr pkgs/${P}-*.tar.gz pkgs/${P}.tar.gz
	PV=$(ls pkgs/${P}-*.tar.gz | sed -e 's/\.tar\.gz//' -e "s/.*${P}-//")
	echo $PV > meta/${P}/version

	mkdir -p bundle/${PN}/
	tar xvf pkgs/${P}.tar.gz --strip-components=1 -C bundle/${PN}/
done

find bundle -type f -name .gitignore -delete
find bundle -type f -name .travis.yml -delete
rm -rf pkgs
