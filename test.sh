#!/bin/sh
set -e

JULIA_LOAD_PATH=$(pwd)/bundle
for P in $(ls bundle); do
	echo julia -e "push!(LOAD_PATH, \"$JULIA_LOAD_PATH\"); using $P;"
	julia -e "push!(LOAD_PATH, \"$JULIA_LOAD_PATH\"); using $P;"
done
